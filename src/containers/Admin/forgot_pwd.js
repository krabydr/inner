import React, {Component} from 'react';
import {connect} from 'react-redux'
import {loginUser} from '../../actions'
import Header from "../../components/Header/header";


import form from '../../scss/components/_form.scss';

class ForgotPWD extends Component {
    state = {
        email: '',
        password: '',
        error: '',
        success: false
    };
    handleInputEmail = (event) => {
        this.setState({
            email: event.target.value
        })
    };

    componentWillReceiveProps(nextProps) {

        if (nextProps.user.login.isAuth) {
            this.props.history.push('/')
        }
    }

    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(loginUser(this.state))
    };

    render() {
        let user = this.props.user;
        const back = {
            backButton: true
        };
        return (
            <React.Fragment>
                <Header name={"Forgot Password"} back={back}/>
                <div className={`${form.login} ${form.reset_password}`}>
                    <form className={form.form} onSubmit={this.submitForm}>
                        <h2>Recover password</h2>
                        <p>Enter your email addres below and we'llget you back on track</p>
                        <div className={form["form-group"]}>
                            <label>Email</label>
                            <input
                                type="email"
                                placeholder="enter your email"
                                value={this.state.email}
                                onChange={this.handleInputEmail}
                            />
                        </div>
                        <button type='submit'>RECOVER</button>
                        <div className='error'>
                            {
                                user.login ?
                                    <div>{user.login.message}</div>
                                    : null
                            }
                        </div>
                    </form>
                </div>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {

    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(ForgotPWD);
