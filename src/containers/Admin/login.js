import React, {Component} from 'react';
import {connect} from 'react-redux'
import {loginUser} from '../../actions'
import {Link} from 'react-router-dom';
import Header from "../../components/Header/header";


import form from '../../scss/components/_form.scss';


class Login extends Component {
    state = {
        email: '',
        password: '',
        error: '',
        success: false
    };
    handleInputEmail = (event) => {
        this.setState({
            email: event.target.value
        })
    };
    handleInputPassword = (event) => {
        this.setState({
            password: event.target.value
        })
    };

    componentWillReceiveProps(nextProps) {

        if (nextProps.user.login.isAuth) {
            this.props.history.push('/user')
        }
    }

    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(loginUser(this.state))
    };

    render() {
        let user = this.props.user;

        return (
            <React.Fragment>
                <Header name={"Login"}/>
                <div className={form.login}>
                    <form className={form.form} onSubmit={this.submitForm}>
                        <h2>Sign in to Inner Circle</h2>
                        <div className={form["form-group"]}>
                            <label>Email</label>
                            <input
                                type="email"
                                placeholder="enter your email"
                                value={this.state.email}
                                onChange={this.handleInputEmail}
                            />
                        </div>
                        <div className={form["form-group"]}>
                            <label>Password</label>
                            <input
                                type="password"
                                placeholder="enter your password"
                                value={this.state.password}
                                onChange={this.handleInputPassword}
                            />
                        </div>
                        <div className={form["reset-pwd"]}>
                            <Link to={"/forgot_pwd"}>
                                Forgot password ?
                            </Link>
                        </div>
                        <button type='submit'>SIGN IN</button>
                        <div className='error'>
                            {
                                user.login ?
                                    <div>{user.login.message}</div>
                                    : null
                            }
                        </div>
                    </form>
                </div>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {

    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Login);
