import React, {Component} from 'react';
import {connect} from "react-redux";
import {generalStatistic, recordList} from "../actions";
import Posts from "../components/Posts/post__items";
import Masonry from "../components/WidgetsUI/masonry_grid";
import GeneraStatistic from "../components/WidgetsUI/statisc_main";
import Skeleton from "../components/WidgetsUI/skeleton";
import compls from '../scss/components/variables/_compls.scss'
import {Col, Container, Row} from "../components/WidgetsUI/container";


class HomeContainer extends Component {
    state = {
        filter: "",
        handlerFilters: "All",
        handlerSearch: " ",
        countPosts: 0,
        error: false,
        hasMore: true,
        isLoading: false,
        statistics: []
    };

    constructor(props) {
        super(props);
        window.onscroll = () => {
            const {
                loadMore,
                state: {
                    error,
                    isLoading,
                    hasMore,
                },
            } = this;
            if (error || isLoading || !hasMore) return;

            if (
                window.innerHeight + document.documentElement.scrollTop
                >= document.documentElement.scrollHeight - 20
            ) {
                loadMore();

            }
        };
    }


    componentWillMount() {
        this.props.dispatch(recordList(10))
            .then(() => {
                this.setState({
                    countPosts: this.props.posts.length
                })
            });
        this.props.dispatch(generalStatistic());

    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setState({
            statistics: nextProps.statistic
        });
    }


    renderItems = (posts) => {
        if (this.state.handlerSearch !== " ") {
            console.log('s');
            this.props.dispatch(recordList(1, 10, 'asc', this.props.posts))// 1: How many items , get curent items , order by asc/desc , set current posts
                .then(() => {
                    this.setState({
                        countPosts: this.props.posts.length,
                        hasMore: (this.state.countPosts < this.props.posts.length),
                        isLoading: false,
                    })
                })
        }
        return posts &&
            posts.map(item => {
                if ((this.props.handlerFilters === "All" && item.title.indexOf(this.props.handlerSearch) > -1)) {
                    return <Posts key={item.id} post={item}/>
                }
                if (item.title === this.props.handlerFilters)
                    return <Posts key={item.id} post={item}/>

                // return <Posts key={item.id} post={item}/>
            })
    };
    loadMore = () => {
        let count = this.props.posts && this.props.posts.length;
        this.setState({
            isLoading: true,
        });
        setTimeout(() => {
            this.props.dispatch(recordList(3, count, 'asc', this.props.posts))// 1: How many items , get curent items , order by asc/desc , set current posts
                .then(() => {
                    this.setState({
                        countPosts: this.props.posts.length,
                        hasMore: (this.state.countPosts < this.props.posts.length),
                        isLoading: false
                    })
                })
        }, 1000);

    };

    render() {
        const statistic = this.state.statistics;
        const {
            hasMore,
            isLoading,
        } = this.state;
        return (
            <Container>
                <Row>
                    <Col mobile={"12"} padding={false}>
                        {statistic && <GeneraStatistic statistics={statistic}/>}
                    </Col>
                    <Col mobile={"12"}>
                        <div className={`${compls.page_block_title}`}>
                            <h3>
                                What’s new
                            </h3>
                        </div>
                        <Masonry>
                            {this.renderItems(this.props.posts)}
                            {(isLoading && hasMore) && <Skeleton/>}
                        </Masonry>
                        {!hasMore &&
                        <div className={compls.endof}>You did it! You reached the end!</div>
                        }
                    </Col>
                </Row>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        posts: state.dashboard.posts,
        statistic: state.dashboard.statistic
    }
}

export default connect(mapStateToProps)(HomeContainer);
