
import React from 'react';
import ReactDom from 'react-dom';
import {BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import promiseMiddleware from 'redux-promise';
import ReduxThunk from 'redux-thunk'
// import '../node_modules/semantic-ui-css/semantic.min.css';
import reducers from './reducers/index';
import Routes from "./routes";
import './index.scss'


const createStoreWithMeddleWare = applyMiddleware(promiseMiddleware,ReduxThunk)(createStore);



ReactDom.render(
<Provider store={createStoreWithMeddleWare(reducers)}>
    <BrowserRouter >
    <Routes/>
    </BrowserRouter>
    </Provider>
    ,document.getElementById('root'));