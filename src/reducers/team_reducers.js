export default function (state = {}, action) {
    switch (action.type) {
        case 'TEAM_USERS':
            return {...state, employee: action.payload};
        default:
            return state;
    }
}