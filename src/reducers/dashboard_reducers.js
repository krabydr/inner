export default function (state = {}, action) {
    switch (action.type) {
        case 'POST_DASHBOARD':
            return {...state, posts: action.payload};
        case 'STATISTIC_DASHBOARD':
            return {...state, statistic: action.payload};
        default:
            return state;
    }
}