export default function (state = {}, action) {
    switch (action.type) {
        case 'PAGE':
            return {...state, page: action.payload};
        default:
            return state;
    }
}