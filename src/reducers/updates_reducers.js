export default function (state = {}, action) {
    switch (action.type) {
        case 'GET_RECORD':
            return {...state, record: action.payload};
        case 'CREATE_RECORD':
            return {
                ...state,
                newRecord: action.payload
            };
        default:
            return state;
    }
}