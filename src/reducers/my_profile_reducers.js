export default function (state = {}, action) {
    switch (action.type) {
        case 'MY_PROFILE':
            return {...state, profile: action.payload};
        case 'EDIT_MY_PROFILE':
            return {...state, profile: action.payload};
        case 'CHANGE_MAIL':
            return {...state, mail: action.payload};
        case 'MY_RECORDS':
            return {...state, my_posts: action.payload};
        default:
            return state;

    }
}