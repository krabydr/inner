import {combineReducers} from 'redux';
import user from './user_reducers';
import dashboard from './dashboard_reducers';
import updates from './updates_reducers';
import team from './team_reducers';
import profile from './my_profile_reducers';
const rootReducer = combineReducers({
    user,
    profile,
    updates,
    dashboard,
    team
});
export default rootReducer;