import axios from 'axios';


/*======USER======*/
export function loginUser({email, password = "password", isAuth = false}) {
    const API = `http://localhost:3000/user?email=${email}&password=${password}`;
    const postAPI = `http://localhost:3000/user`;
    const request = axios.get(API, {isAuth, email, password})
        .then(response => {
            console.log(response.data);
            const id = response.data[0].id;
            axios.put(`${postAPI}/${id}`, {
                "id": "1",
                "isAuth": true,
                "email": "zver@gmail.com",
                "password": "password"
            })
                .then(response => {
                    console.log(`${postAPI}?id=${id}`)
                })
                .catch(err => console.log(err))
        });
    return {
        type: 'USER_LOGIN',
        payload: request
    }
}

export function auth() {
    const API = 'http://localhost:3000/user/1';
    const request = axios.get(API)
        .then(response => response.data);
    return {
        type: 'USER_AUTH',
        payload: request
    }
}


// DASHBOARD
// Record list   for display all post from api
export function recordList(limit = 10, start = 0, order = 'asc', list = '') {
    const API = `http://localhost:3000/posts?_limit=${limit}&_start=${start}&_sort=name&_order=${order}`;
    const request = axios.get(API)
        .then(response => {
                if (list) {

                    return [...list, ...response.data]
                } else {
                    return response.data
                }
            }
        );
    return {
        type: 'POST_DASHBOARD',
        payload: request
    }
}

// Get record for updates
export function getRecord(id) {
    const request = axios.get(`http://localhost:3000/posts?id=${id}`)
        .then(response => {

                if (response.data.length < 1) {
                    return response.data = false
                } else {
                    return response.data
                }

            }
        )
    return {
        type: 'GET_RECORD',
        payload: request
    }
}

// POST new record
export function createNewRecord(record) {
    console.log(record)
    const request = axios.post('http://localhost:3000/posts', record)
        .then(response => response.data);

    return {
        type: 'CREATE_RECORD',
        payload: request
    }
}

// General statistic  for display data to dashboard
export function generalStatistic() {
    const API = 'http://localhost:3000/dashboard';
    const request = axios.get(API)
        .then(response => response.data);
    return {
        type: 'STATISTIC_DASHBOARD',
        payload: request
    }
}


//TEAM

// Team list  getTeamUsers
export function teamList() {
    const API = 'http://localhost:3000/team';

    const request = axios.get(API)
        .then(response => response.data);
    return {
        type: 'TEAM_USERS',
        payload: request
    }
}


// My Profile

export function getMyProfile(id) {
    const API = `http://localhost:3000/users_profile?id=${id}`;
    const request = axios.get(API)
        .then(response => response.data);
    return {
        type: 'MY_PROFILE',
        payload: request
    }
}

export function editProfile(profile, id) {
    const API = `http://localhost:3000/users_profile?id=${id}`;
    const request = axios.put(API, profile)
        .then(response => response.data);
    return {
        type: 'EDIT_MY_PROFILE',
        payload: request
    }
}

export function changeMail(profile, id) {
    const API = `http://localhost:3000/change_email?id=${id}`;
    const request = axios.put(API, profile)
        .then(response => response.data);
    return {
        type: 'CHANGE_MAIL',
        payload: request
    }
}

export function myRecordList() {
    const API = `http://localhost:3000/posts?user_id=1`;
    const request = axios.get(API)
        .then(response => response.data);
    return {
        type: 'MY_RECORDS',
        payload: request
    }
}
