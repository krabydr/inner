import React, {Component} from 'react';
import HomeContainer from "../../containers/home_container";
import Header from "../Header/header";


class Home extends Component {
    state = {
        handlerFilters: "All",
        handlerSearch: ""
    };
    handlerFilters = (value) => this.setState({handlerFilters: value});
    handlerSearch = (value) => this.setState({handlerSearch: value});

    render() {

        const props = this.props;
        const filters = {
            search: true,
            show: true
        };

        return (
            <React.Fragment>
                <Header name={"Dashboard"} filters={filters} handlerFilters={this.handlerFilters}
                        handlerSearch={this.handlerSearch}/>
                <HomeContainer
                    props={props}
                    handlerFilters={this.state.handlerFilters}
                    handlerSearch={this.state.handlerSearch}
                />
            </React.Fragment>
        );
    }
}


export default Home;
