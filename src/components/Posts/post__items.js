import React from 'react';
import {Link} from 'react-router-dom'
import grid_post from '../../scss/components/_masonry.scss';

const Posts = (props) => {
    const post = props.post;
    let template = '';
    switch (post.name) {
        case post.name:
            return template = (
                <div className={`${grid_post.column_item}`}>
                    <figure>
                        <img src="https://picsum.photos/200/90/?random" height={90} alt=""/>
                    </figure>
                    <h3>
                        {post.title}
                    </h3>
                    <h5>
                        {post.preview}
                    </h5>

                    <div className={`${grid_post.column_item_bottom}`}>
                        <button>
                            <Link to={`/updates/${post.id} `}>Get direction</Link>
                        </button>
                    </div>
                </div>
            );
        case 2:
            return false;
        default:
            return template;
    }
};

export default Posts;
