import React from 'react';
import styles from '../../scss/components/variables/container.scss';
import grid from '../../scss/components/_grid.scss';


const Container = props => {
    return (
        <div className={styles.container}>
            {props.children}
        </div>
    );
};
const Row = props => {
    const {padding} = props;

    return (
        <div className={`${grid.row} ${grid[padding ? true : "no-padding"]} `}>
            {props.children}
        </div>
    );
};
const Col = props => {
    const {mobile,padding} = props;
    return (
        <div className={`${grid[`col-xs-` + mobile]}  ${grid[padding ? true : "no-padding"]}`}>
            {props.children}
        </div>
    );
};

export {
    Container,
    Row,
    Col

}