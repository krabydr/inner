import React from 'react';
import styles from '../../scss/components/_masonry.scss';


const Masonry = props => {
    return (
        <div className={styles.masonry}>
            {props.children}
        </div>
    );
};
export default Masonry;
