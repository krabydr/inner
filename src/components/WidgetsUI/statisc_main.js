import React from 'react';
import Moment from 'react-moment';

import comps from '../../scss/components/variables/_compls.scss';
import style_statistics from '../../scss/components/_statistics.scss';
import grid from '../../scss/components/_grid.scss';

Moment.globalFormat = 'LTS';
Moment.globalElement = 'span';

const GeneraStatistic = (one) => {

    let {id, total_users, online_users, updates, articles, comments} = one.statistics;
    const percent = (online_users / total_users) * 100;
    const dateToFormat = '1370001284';
    return (
        <div className={`${grid.row} ${grid["no-margin"]} `}>
            <div className={`${grid["col-xs-8"]}`}>
                <h4>
                    General statistics
                </h4>
                <div className={`${style_statistics.general_statistic} ${comps.box}`}>
                    <div className={style_statistics.statistic_text}>
                        <div className={style_statistics.statistic_header}>
                            <div className={style_statistics.statistic_header_inner}>
                                <h6>Total Users</h6>
                                <span>{total_users}</span>
                            </div>
                            <div className={style_statistics.statistic_header_inner}>
                                <h6>Online Users</h6>
                                <span>{online_users}</span>
                            </div>
                            <div className={style_statistics.statistic_header_inner}>
                                <h6>New Users</h6>
                                <span>{one.statistics.new}</span>
                            </div>
                        </div>
                        <div className={style_statistics.statistic_body}>
                            <div>
                                <h6>UPDATES</h6>
                                <span>{updates}</span>
                            </div>
                            <div>
                                <h6>Files</h6>
                                <span>{articles}</span>
                            </div>
                            <div>
                                <h6>Events</h6>
                                <span>{comments}</span>
                            </div>
                            <div>
                                <h6>UPDATES</h6>
                                <span>{updates}</span>
                            </div>
                            <div>
                                <h6>Files</h6>
                                <span>{articles}</span>
                            </div>
                            <div>
                                <h6>Events</h6>
                                <span>{comments}</span>
                            </div>
                        </div>
                    </div>
                    <div className={style_statistics.statistic_circle}>
                        <div className={style_statistics["single-chart"]}>
                            <svg viewBox="0 0 36 36" className={style_statistics["circular-chart"]}>
                                <path className={style_statistics["circle-bg"]}
                                      d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"/>
                                <path className={style_statistics.circle} strokeDasharray={percent + ", 100"}
                                      d="M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831"/>
                                <text x="17 " y="20.35"
                                      className={style_statistics.percentage}>{String(percent.toFixed(0))}
                                </text>
                                <text  x="24" y="18.35"  className={style_statistics.percentage_item}>%</text>
                            </svg>
                        </div>
                        <div className={style_statistics.count_online}>
                            <div className={style_statistics.online}>
                                <span className={style_statistics.online_item}><strong>Online</strong></span>
                                <span>{online_users} users</span>
                            </div>
                            <div className={style_statistics.offline}>
                                <span className={style_statistics.online_item}><strong>Offline</strong></span>
                                <span>{online_users && total_users - online_users} users</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={`${grid["col-xs-4"]}`}>
                <h4>
                    My statistics
                </h4>
                <div className={`${style_statistics.my_statistics_wrapper} ${comps.box} `}>
                    <div className={style_statistics.my_statistics}>
                        <h6>Time online</h6>
                        <div className={style_statistics.my_statistics_inner}>
                            <Moment unix format={`h [h :] m [m : ] ss [s]`}>{dateToFormat}</Moment>
                        </div>
                    </div>
                    <div className={style_statistics.my_statistics}>
                        <div className={style_statistics.my_statistics_inner}>
                            <h6>Update postst</h6>
                            <span className={style_statistics.my_statistics_item}>
                                    {id}
                                <span className={style_statistics.my_statistics_item_symbol}>{id} this month</span>
                                </span>
                        </div>
                        <div className={style_statistics.my_statistics_inner}>
                            <h6>Files upload</h6>
                            <span className={style_statistics.my_statistics_item}>
                                    {id}
                                <span className={style_statistics.my_statistics_item_symbol}>{id} this month</span>
                                </span>
                        </div>
                    </div>
                    <div className={style_statistics.my_statistics}>
                        <h6>Profile complete</h6>
                        <span className={style_statistics.progress_bars}>
                                83%
                            </span>
                        <div className={style_statistics.progress_bar}>
                            <span style={{width: "83%"}}> </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
        ;
};

export default GeneraStatistic;
