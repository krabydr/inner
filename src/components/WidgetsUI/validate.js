const validateField = (fieldName, value, state) => {

    let fieldValidationErrors = state.formErrors;
    // let emailValid = state.emailValid;
    let firstNameValid = state.firstNameValid;

    switch (fieldName) {
        // case 'email':
        //     emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        //     fieldValidationErrors.email = emailValid ? '' : ' is invalid';
        //     break;
        case 'first_name':
            firstNameValid = value.length >= 6;
            fieldValidationErrors.first_name = firstNameValid ? '' : ' is too short';
            break;
        default:
            break;
    }
    const date = {fieldValidationErrors, firstNameValid};
    // this.setState({
    //     formErrors: fieldValidationErrors,
    //     emailValid: emailValid,
    //     firstNameValid: firstNameValid
    // }, this.validateForm);
    return date;
}

export default validateField;