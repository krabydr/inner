import React from 'react';
import skeleton from "../../scss/components/_skeleton.scss";
const Skeleton = props => {

    let skeletons = [];

    for (let i = 0; i < 3; i++) {
        skeletons.push(
            <div className={skeleton.skeleton} key={i}>
                <div className={skeleton.skeleton_img}/>

                <div className={skeleton.skeleton_title}/>
                <div className={skeleton.skeleton_title}/>

                <div className={skeleton.skeleton_text}/>
                <div className={skeleton.skeleton_text}/>
                <div className={skeleton.skeleton_text}/>

                <div className={skeleton.skeleton_button}/>

            </div>
        );
    }

    return (
        <React.Fragment>
            {skeletons}
        </React.Fragment>
    );

};


export default Skeleton;
