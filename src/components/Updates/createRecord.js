import React, {Component} from 'react';
import {connect} from 'react-redux';

import {createNewRecord} from '../../actions'
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Header from "../Header/header";
import form from '../../scss/components/_form.scss';
import {Col, Container, Row} from "../WidgetsUI/container";

class createRecord extends Component {

    state = {
        form_data: {
            title: "",
            preview: "",
            category_name: "",
            category_id: null,
            tags: [],
            text: "",
            comments_count: null,
            time: null
        }
    };
    handleInputADD = (event) => {
        this.submitForm(event);
    };
    handleInput = (event, name) => {
        const newFormDate = {
            ...this.state.form_data
        };
        newFormDate[name] = (event.target) ? event.target.value : event;
        this.setState({
            form_data: newFormDate
        })
    };


    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(createNewRecord({
            ...this.state.form_data,
            user_id: this.props.user.login.id
        }))
            .then(() => {
                this.props.history.push("/updates");
            })
    };

    render() {
        const filters = {
            search: true
        };
        const button = {
            text: "Create New",
            link: "/updates",
            visibility: true

        };
        return (
            <React.Fragment>
                <Header name={"Create New"} filters={filters} addButton={button} handelerSubmit={this.handleInputADD}/>
                <Container>
                    <Row>
                        <Col mobile={"8"}>
                            <form className={form.form}>
                                <div className={form["form-group"]}>
                                    <label>Title</label>
                                    <input type="text" placeholder="Enter title" value={this.state.form_data.title}
                                           onChange={(event) => this.handleInput(event, 'title')}/>
                                </div>
                                <div className={form["form-group"]}>
                                    <label>Preview</label>
                                    <input type="text" placeholder="Enter preview" value={this.state.form_data.preview}
                                           onChange={(event) => this.handleInput(event, 'preview')}/>
                                </div>
                                <div className={form["form-group"]}>
                                    <label>Category name</label>
                                    <input type="text" placeholder="Enter category name"
                                           value={this.state.form_data.category_name}
                                           onChange={(event) => this.handleInput(event, 'category_name')}/>
                                </div>

                                <CKEditor
                                    editor={ClassicEditor}
                                    data={this.state.form_data.text}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        this.handleInput(data, 'text')
                                    }}
                                />
                            </form>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }

    componentWillUnmount() {
        // this.props.dispatch(clearBook())
    }
}

function mapStateToProps(state) {
    return {
        newRecord: state.newRecord
    }
}

export default connect(mapStateToProps)(createRecord);
