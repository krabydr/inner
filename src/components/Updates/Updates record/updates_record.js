import React, {Component} from 'react';
import {getRecord} from '../../../actions'
import {connect} from 'react-redux';
import HeaderMain from "../../Header/header";
import {Col, Container, Row} from "../../WidgetsUI/container";

class UpdatesRecord extends Component {


    componentWillMount() {
        this.props.dispatch(getRecord(this.props.match.params.id));
    }


    componentWillUnmount() {
        // this.props.dispatch(clearBookWithReviewer());
    }

    renderRecord = (record) => {
        if (record) {
            return (
                <Row>
                    <Col mobile={"12"}>
                        <h1>{record[0] && record[0].title}</h1>
                        <h3>{record[0] && record[0].preview}</h3>
                        <div dangerouslySetInnerHTML={{__html: record[0] && record[0].text}}/>
                    </Col>
                </Row>
            )
        } else {
            return (
                <Row>
                    <Col mobile={"12"}>
                        <h2>
                            Ops ! We dont have this update
                        </h2>
                    </Col>
                </Row>
            )
        }
    };

    render() {

        let record = this.props.record;
        return (
            <React.Fragment>
                <HeaderMain name={"Updates"}/>
                <Container>
                    {this.renderRecord(record)}
                </Container>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        record: state.updates.record
    }
}

export default connect(mapStateToProps)(UpdatesRecord);
