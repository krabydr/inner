import React, {Component} from 'react';

import Posts from "../Posts/post__items";

import {recordList} from "../../actions";
import {connect} from "react-redux";
import Header from "../Header/header";
import Skeleton from '../../components/WidgetsUI/skeleton'
import Masonry from "../../components/WidgetsUI/masonry_grid";
import {Container} from "../WidgetsUI/container";
import compls from "../../scss/components/variables/_compls.scss";


class Updates extends Component {

    state = {
        filter: "",
        handlerFilters: "All",
        handlerSearch: "",
        countPosts: 0,
        error: false,
        hasMore: true,
        isLoading: false,
    };

    constructor(props) {
        super(props);
        window.onscroll = () => {
            const {
                loadMore,
                state: {
                    error,
                    isLoading,
                    hasMore,
                },
            } = this;
            if (error || isLoading || !hasMore) return;

            if (
                window.innerHeight + document.documentElement.scrollTop
                >= document.documentElement.scrollHeight - 100
            ) {
                loadMore();

            }
        };
    }


    componentWillMount() {
        this.props.dispatch(recordList(13))
            .then(() => {
                this.setState({
                    countPosts: this.props.posts.length
                })
            })

    }

    handlerFilters = (value) => this.setState({handlerFilters: value});
    handlerSearch = (value) => this.setState({handlerSearch: value});


    renderItems = (posts) => (
        posts &&
        posts.map(item => {
            if (this.state.handlerFilters === "All" && item.title.indexOf(this.state.handlerSearch) > -1) {
                return <Posts key={item.id} post={item}/>
            }
            if (item.title === this.state.handlerFilters)
                return <Posts key={item.id} post={item}/>

        })
    );
    loadMore = () => {
        let count = this.props.posts.length;
        this.setState({
            isLoading: true,
        });
        setTimeout(() => {
            this.props.dispatch(recordList(3, count, 'asc', this.props.posts))// 1: How many items , get curent items , order by asc/desc , set current posts
                .then(() => {
                    this.setState({
                        countPosts: this.props.posts.length,
                        hasMore: (this.state.countPosts < this.props.posts.length),
                        isLoading: false,
                    })
                })
        }, 1000);

    };

    render() {
        const filters = {
            search: true,
            show: true,
            tags: true
        };
        const button = {
            text: "New",
            link: "/update/create",
            visibility: true
        };
        const {hasMore, isLoading} = this.state;
        return (
            <React.Fragment>
                <Header
                    name={"Updates"}
                    filters={filters}
                    addButton={button}
                    handlerFilters={this.handlerFilters}
                    handlerSearch={this.handlerSearch}
                />
                <Container>
                    <Masonry>
                        {this.renderItems(this.props.posts)}
                        {(isLoading) && <Skeleton/>}

                    </Masonry>
                    {!hasMore && <div className={compls.endof}>You did it! You reached the end!</div>}
                </Container>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {


    return {
        posts: state.dashboard.posts
    }
}

export default connect(mapStateToProps)(Updates);
