import React, {Component} from 'react';
import Header from "../Header/header";

import {editProfile, getMyProfile} from "../../actions";
import {connect} from "react-redux";
import ProfileItems from "./Profile_sidebar/profile_items";
import form from '../../scss/components/_form.scss';
import {Col, Container, Row} from "../WidgetsUI/container";
import validate from "../WidgetsUI/validate";

class MyAccount extends Component {

    constructor(props) {
        super(props);
        this.validate = validate;
    }


    state = {
        form_data: {
            id: "",
            first_name: "",
            last_name: "",
            birthday: "",
            office: "",
            position: "",
            department: "",
            country: "",
            city: "",
            avatar: ""
        }
        ,
        formErrors: {
            id: "",
            first_name: "",
            last_name: "",
            birthday: "",
            office: "",
            position: "",
            department: "",
            country: "",
            city: "",
            avatar: ""
        },
        firstNameValid: null,
        lastNameValid: null,
        birthdayValid: null,
        officeValid: null,
        positionValid: null,
        departmentValid: null,
        countryValid: null,
        cityValid: null,
        formValid: null
    };

    componentDidMount() {

        const user = this.props.user.login.id;


        this.props.dispatch(getMyProfile(user))
            .then(() => {
                let {id, first_name, last_name, birthday, office, position, department, country, city} = this.props.profile;
                this.setState({
                    form_data: {id, first_name, last_name, birthday, office, position, department, country, city}
                });
            });
    }

    handleInput = (event, name) => {
        const newFormDate = {
            ...this.state.form_data
        };

        let value = event.target.value;
        const validData = this.validate(name, value, this.state);
        newFormDate[name] = (event.target) ? value : event;
        this.setState({
            form_data: newFormDate
        }, () => {
            // this.validateField(name, value)
            this.setState({
                ...validData
            }, this.validateForm);
        });



    };
    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(editProfile({
            ...this.state.form_data,
        }, this.props.user.login.id))
            .then(() => {
                alert("Your account successful changed")
            })
    };


    validateForm() {
        this.setState({
            formValid: this.state.firstNameValid
        });
    }

    render() {
        return (
            <React.Fragment>
                <Header name={"My Account"}/>
                <Container>
                    <Row>
                        <Col mobile={"9"}>
                            <form className={form.form} onSubmit={this.submitForm}>
                                <div className={form["form-group"]}>
                                    <label>First Name</label>
                                    <input
                                        type="text"
                                        placeholder="Enter title"
                                        className={!this.state.firstNameValid &&  this.state.firstNameValid !== null ? `${form["form-invalid"]}` : null}
                                        value={this.state.form_data.first_name}
                                        onChange={(event) => this.handleInput(event, 'first_name')}/>
                                </div>
                                <div className={form["form-group"]}>
                                    <label>Seond Name</label>
                                    <input type="text" placeholder="Enter title"
                                           value={this.state.form_data.last_name}
                                           onChange={(event) => this.handleInput(event, 'last_name')}/>
                                </div>
                                <div className={form["form-group"]}>
                                    <label>Your birthday</label>
                                    <input type="text" placeholder="Enter title"
                                           value={this.state.form_data.birthday}
                                           onChange={(event) => this.handleInput(event, 'birthday')}/>

                                </div>
                                <div className={form["form-group"]}>
                                    <label>Office</label>
                                    <input type="text" placeholder="Enter title" value={this.state.form_data.office}
                                           onChange={(event) => this.handleInput(event, 'office')}/>
                                </div>
                                <div className={form["form-group"]}>
                                    <label>Position</label>
                                    <input type="text" placeholder="Enter title"
                                           value={this.state.form_data.position}
                                           onChange={(event) => this.handleInput(event, 'position')}/>

                                </div>
                                <div className={form["form-group"]}>
                                    <label>Department</label>

                                    <input

                                        type="text"
                                        placeholder="Enter title"
                                        value={this.state.form_data.department}
                                        onChange={(event) => this.handleInput(event, 'department')}/>
                                </div>
                                <div className={form["form-group"]}>
                                    <label>Country</label>
                                    <input type="text" placeholder="Enter title"
                                           value={this.state.form_data.country}
                                           onChange={(event) => this.handleInput(event, 'country')}/>

                                </div>
                                <div className={form["form-group"]}>
                                    <label>City</label>
                                    <input type="text" placeholder="Enter title" value={this.state.form_data.city}
                                           onChange={(event) => this.handleInput(event, 'city')}/>
                                </div>
                                <button type='submit'>Submit</button>
                            </form>
                        </Col>
                        <Col mobile={"3"}>
                            <ProfileItems user={this.props.user.login}/>
                        </Col>
                    </Row>
                </Container>

            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {

    return {
        profile: state.profile.profile
    }
}

export default connect(mapStateToProps)(MyAccount);

