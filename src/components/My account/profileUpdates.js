import React, {Component} from 'react';
import HeaderMain from "../Header/header";
import ProfileItems from "./Profile_sidebar/profile_items";
import {connect} from "react-redux";
import {myRecordList} from "../../actions";
import grid from '../../scss/components/_grid.scss';
import comps from '../../scss/components/variables/_compls.scss';
import table from "../../scss/components/_table.scss";
import {Container} from "../WidgetsUI/container";


// import {getMyProfile} from "../../actions";


class ProfileUpdates extends Component {
    state = {
        mail_data: {
            id: "",
            current_mail: "",
            new_mail: "",
            new_mail_confirm: "",
        },
        pass_data: {
            id: "",
            current_password: "",
            new_password: "",
            new_password_confirm: "",
        },
        updates: {
            id: null,
            checked: false
        }


    };

    componentWillMount() {
        this.props.dispatch(myRecordList())
            .then(() => {
                this.setState({
                    updates: [...this.props.posts]
                });
            });


    }


    handleCheck(e, id) {
        this.setState(state => {
            const list = state.updates.map((item) => {
                if (item.id === id) {
                    return {...item, publish: true}
                } else {
                    return item;
                }
            });
            return {
                updates: list,
            };
        });
    }

    renderUpdates = (update) => (
        update &&
        update.map(item => (
            <React.Fragment key={item.id}>
                <div className={`${table["table-row-body"]} ${table["profile"]} ${table["border_bottom"]}`}
                     style={{gridTemplateColumns: "0.5fr 2fr 1fr 1fr 0.3fr"}}>
                    <figure>
                        <img src="https://picsum.photos/200/90/?random" alt=""/>
                    </figure>
                    <div>
                        {item.title}
                    </div>
                    <div>
                        {item.time}
                    </div>
                    <div>
                        <div className={`${comps.switch}`}>
                            <label htmlFor={`switch${item.id}`}
                                   className={`${comps.switch_lbl} ${(item.publish) ? comps.active : comps.unactive}`}>Status</label>
                            <input
                                type="checkbox"
                                id={`switch${item.id}`}
                                name="set-name"

                                className={`${comps.switch_inp} `}
                                checked={this.state.checked}
                                onChange={(e) => this.handleCheck(e, item.id)}
                            />
                            {console.log(item.publish)}
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </React.Fragment>
        ))
    );

    render() {
        // console.log(this.state.updates);
        return (
            <React.Fragment>
                <HeaderMain name={"My Account"}/>
                <Container>
                    <div className={grid.row}>
                        <div className={grid["col-xs-9"]}>
                            <div className={comps.segment}>
                                <h3>Your updates</h3>
                                <div className={grid.row}>
                                    <div className={grid["col-xs-12"]}>
                                        <div
                                            className={`${table["table-row"]} ${table["table-row-header"]} ${table["border_bottom"]}`}
                                            style={{gridTemplateColumns: "0.5fr 2fr 1fr 1fr 0.3fr"}}>
                                            <figure>

                                            </figure>
                                            <p> Title </p>
                                            <p> Date </p>
                                            <p> Status </p>
                                            <p> Action </p>
                                        </div>
                                        <div className={table["table-row"]}>
                                            {this.renderUpdates(this.props.posts)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={grid["col-xs-3"]}>
                            <ProfileItems user={this.props.user.login}/>
                        </div>
                    </div>
                </Container>

            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        posts: state.profile.my_posts
    }
}

export default connect(mapStateToProps)(ProfileUpdates);