import React, {Component} from 'react';
import HeaderMain from "../Header/header";
import {Form} from "semantic-ui-react";
import ProfileItems from "./Profile_sidebar/profile_items";
import {connect} from "react-redux";
// import {changeMail, getMyProfile} from "../../actions";

import grid from '../../scss/components/_grid.scss';
import form from '../../scss/components/_form.scss';
import comps from '../../scss/components/variables/_compls.scss';
import {Container,Col, Row} from "../WidgetsUI/container";

class ChangeMail extends Component {
    state = {
        mail_data: {
            id: "",
            current_mail: "",
            new_mail: "",
            new_mail_confirm: "",
        },
        pass_data: {
            id: "",
            current_password: "",
            new_password: "",
            new_password_confirm: "",
        }
    };


    handleInput = (event, name, type) => {
        if (type === "mail") {
            const newEmail = {
                ...this.state.mail_data
            };

            newEmail[name] = (event.target) ? event.target.value : event;
            this.setState({
                mail_data: newEmail
            })
        } else {
            const newPass = {
                ...this.state.pass_data
            };
            newPass[name] = (event.target) ? event.target.value : event;
            this.setState({
                mail_data: newPass
            })
        }

    };
    submitForm = (e, type) => {
        e.preventDefault();
        if (type === "mail") {
            // this.props.dispatch(changeMail({
            //     ...this.state.mail_data,
            // }, this.props.user.login.id))
            //     .then(() => {
            //         alert("Your account successful changed")
            //     })
            console.log(this.state.mail_data)
        } else {
            console.log(this.state.pass_data)
        }
        // this.props.dispatch(editProfile({
        //     ...this.state.form_data,
        // }, this.props.user.login.id))
        //     .then(() => {
        //         alert("Your account successful changed")
        //     })
    };

    render() {
        return (
            <React.Fragment>
                <HeaderMain name={"My Account"}/>
                <Container>
                    <Row>
                        <Col mobile={"9"}>
                            <div className={comps.segment}>
                                <h3>
                                    Change profile password
                                </h3>
                                <form className={form.form} onSubmit={(event) => this.submitForm(event, 'mail')}>
                                    <div className={`${form["form-group"]} ${grid["col-xs-6"]} ${form["row-direct"]}`}>

                                        <label>Current password</label>
                                        <input type="text" placeholder="Enter title"
                                               value={this.state.mail_data.current_mail}
                                               onChange={(event) => this.handleInput(event, 'current_mail', 'mail')}/>
                                    </div>
                                    <div className={`${form["form-group"]} ${grid["col-xs-6"]} ${form["row-direct"]}`}>

                                        <label>New password</label>
                                        <input type="text" placeholder="Enter title"
                                               value={this.state.mail_data.new_mail}
                                               onChange={(event) => this.handleInput(event, 'new_mail', 'mail')}/>

                                    </div>
                                    <div className={`${form["form-group"]} ${grid["col-xs-6"]} ${form["row-direct"]}`}>

                                        <label>Confirm password</label>
                                        <input type="text" placeholder="Enter title"
                                               value={this.state.mail_data.new_mail_confirm}
                                               onChange={(event) => this.handleInput(event, 'new_mail_confirm', 'mail')}/>

                                    </div>
                                    <button type='submit'>Submit</button>
                                </form>
                            </div>
                            <div className={comps.segment}>
                                <h3>
                                    Change profile mail
                                </h3>
                                <Form onSubmit={(event) => this.submitForm(event, 'pass')}>
                                    <div className={`${form["form-group"]} ${grid["col-xs-6"]} ${form["row-direct"]}`}>
                                        <label>Current email</label>
                                        <input type="text" placeholder="Enter title"
                                               value={this.state.pass_data.current_password}
                                               onChange={(event) => this.handleInput(event, 'current_password')}/>
                                    </div>
                                    <div className={`${form["form-group"]} ${grid["col-xs-6"]} ${form["row-direct"]}`}>
                                        <label>New email</label>
                                        <input type="text" placeholder="Enter title"
                                               value={this.state.pass_data.new_password}
                                               onChange={(event) => this.handleInput(event, 'new_password')}/>
                                    </div>
                                    <div className={`${form["form-group"]} ${grid["col-xs-6"]} ${form["row-direct"]}`}>
                                        <label>Confirm email</label>
                                        <input type="text" placeholder="Enter title"
                                               value={this.state.pass_data.new_password_confirm}
                                               onChange={(event) => this.handleInput(event, 'new_password_confirm')}/>
                                    </div>
                                    <button type='submit'>Submit</button>
                                </Form>
                            </div>
                        </Col>
                        <Col mobile={"3"}>
                            <ProfileItems user={this.props.user.login}/>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        profile: state.profile.profile
    }
}

export default connect(mapStateToProps)(ChangeMail);