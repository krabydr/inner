import React from 'react';
import {Link} from 'react-router-dom';
import {envelopeO} from 'react-icons-kit/fa/envelopeO';

import links from '../../../scss/components/_my_acc.scss';
import Icon from 'react-icons-kit';
import compls from "../../../scss/components/variables/_compls.scss";

const ProfileItems = ({user}) => {
    /* Docs
     Items: object with all routs
        type: Class for css
        text: Link text
        link: Route link
        restricted: Not logged user can see this item
        accesses: Are this block have been pay

     */
    const items = [
        {
            type: 'navItem',
            text: 'Profile Settings',
            link: '/my_account',
            icon: envelopeO,
            restricted: false,
            accesses: true
        },
        {
            type: 'navItem',
            text: 'Password / Email',
            link: '/my_account/change_mail',
            icon: envelopeO,
            restricted: false,
            accesses: true
        },
        {
            type: 'navItem',
            text: 'Updates',
            link: '/my_account/updates',
            icon: envelopeO,
            restricted: false,
            accesses: true
        },
        {
            type: 'navItem',
            text: 'Logout',
            link: '/my_account/logout',
            icon: envelopeO,
            restricted: false,
            accesses: true
        }
    ];


    const element = (item, i, url) => (
        <div key={i} className={`${links[item.type]} ${item.link === url ? links.active : null} `}>
            <Link to={item.link}>
                <Icon icon={item.icon} className={links.icon}/>
                <span>{item.text}</span>
            </Link>
        </div>
    );
    const ProfileItem = () => {
        const url = window.location.pathname;
        return user ?
            items.map((item, i) => {
                if (user.isAuth && item.accesses) {
                    return element(item, i, url);
                } else {
                    return item.restricted ? element(item, i, url) : null;
                }
                // return element(item, i)
            })
            : null
    };

    return (
        <React.Fragment>
            {ProfileItem()}
        </React.Fragment>
    );
};

export default ProfileItems;



