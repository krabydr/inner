import React from 'react';
import Header from "../../components/Header/header";
import {Col, Container, Row} from "../WidgetsUI/container";
// import axios from 'axios'

const Logout = (props) => {
    // let requset = axios.get('/api/logout')
    //     .then(request => {
    setTimeout(() => {
        props.history.push('/')
    }, 2000);
    // });
    return (
        <React.Fragment>
            <Header
                name={"Logout"}
            />
            <Container>
                <Row>
                    <Col>
                        <h1>
                            Sorry to see you go
                        </h1>
                    </Col>
                </Row>
            </Container>


        </React.Fragment>
    );
};

export default Logout;
