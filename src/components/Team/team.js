import React, {Component} from 'react';
import HeaderMain from "../Header/header";
// import {Table} from "semantic-ui-react";
import {teamList} from "../../actions";
import {connect} from "react-redux";
// import {Link} from 'react-router-dom'
import Employees from "./Employees/employees";

import table from '../../scss/components/_table.scss';
import {Col, Container, Row} from "../WidgetsUI/container";

class Team extends Component {

    state = {
        handlerFilters: "All",
        handlerSearch: " ",
    };


    componentWillMount() {
        this.props.dispatch(teamList());
    }

    handlerFilters = (value) => this.setState({handlerFilters: value});
    handlerSearch = (value) => this.setState({handlerSearch: value});
    renderEmployees = (employee) => (
        employee &&
        employee.map(item => {
            if (this.state.handlerFilters === "All" && item.name.indexOf(this.state.handlerSearch) > -1)
                return <Employees key={item.id} employees={item}/>
            if (item.title === this.state.handlerFilters)
                return <Employees key={item.id} employees={item}/>
        })
    );

    render() {
        const filters = {
            search: true,
            show: true,
            tags: true,
            sort: false
        };
        const button = {
            text: "New",
            link: "/update/create",
            visibility: true
        };
        return (
            <React.Fragment>
                <HeaderMain
                    name={"Team"}
                    filters={filters}
                    addButton={button}
                    handlerFilters={this.handlerFilters}
                    handlerSearch={this.handlerSearch}
                />
                <Container>
                    <Row>
                        <Col mobile={"12"}>
                            <div
                                className={`${table["table-row"]} ${table["table-row-header"]} ${table["border_bottom"]}`}
                                style={{gridTemplateColumns: "0.2fr 1fr 1fr 1fr 0.5fr 0.5fr 0.5fr"}}>
                                <div>

                                </div>
                                <div>
                                    Name
                                </div>
                                <div>
                                    Position
                                </div>
                                <div>
                                    Department
                                </div>
                                <div>
                                    City
                                </div>
                                <div>
                                    Activity
                                </div>
                                <div>
                                    Actions
                                </div>
                            </div>
                            <div className={table["user-row"]}>
                                {this.renderEmployees(this.props.employee)}
                            </div>
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        employee: state.team.employee
    }
}

export default connect(mapStateToProps)(Team);

