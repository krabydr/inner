import React from 'react';

import {Link} from 'react-router-dom'
//Icons
import Icon from 'react-icons-kit';

import {envelopeO} from 'react-icons-kit/fa/envelopeO'
import {ic_comment} from 'react-icons-kit/md/ic_comment'


import table from "../../../scss/components/_table.scss";
import compls from "../../../scss/components/variables/_compls.scss";


const Employees = (props) => {
    const {name, activity, department, city, postions} = props.employees;

// const template =
    return (
        <div className={`${table["table-row-body"]} ${table["team"]} ${table["border_bottom"]}`}
             style={{gridTemplateColumns: "0.2fr 1fr 1fr 1fr 0.5fr 0.5fr 0.5fr"}}>
            <figure>
                <img src="https://picsum.photos/40/40/?random" alt=""/>
            </figure>
            <div>
                <p><strong>{name}</strong></p>
            </div>
            <div>
                <p>{postions}</p>
            </div>
            <div className={table.department}>
                <p>{department}</p>
            </div>
            <div>
                <p>{city}</p>
            </div>
            <div>
                <p>{activity}</p>
            </div>
            <div>
                <div className={compls.icon}>
                    <Link to={"/"}>
                        <Icon icon={envelopeO}/>

                    </Link>
                    <Link to={"/"}>
                        <Icon icon={ic_comment}/>
                    </Link>

                </div>
            </div>
        </div>
    );
};

export default Employees;
