import React, {Component} from 'react';
// import styles from '../../scss/style.scss'
// import {auth} from "../../actions";
import {connect} from "react-redux";
import {Link} from 'react-router-dom';


import Dropdown from "../WidgetsUI/dropdown";
import Search from "../WidgetsUI/search";

import styles from '../../scss/components/_page_header.scss';

class Header extends Component {

    state = {
        page__name: "Dashboard",
        sort: false,
        back: false,
        show: false,
        tags: false,
        search: false,
        addButton: {
            text: "null"
        }
    };

    componentWillMount() {
        let {name, filters, addButton, back} = this.props;

        this.setState({
            page__name: name,
            search: filters ? filters.search : null,
            show: filters ? filters.show : null,
            tags: filters ? filters.tags : null,
            addButton: addButton,
            back
        });

    }

    handlerSearch = (val) => {
        this.props.handlerSearch(val)
    };
    onSelectSort = (value) => {
        this.props.handlerFilters(value)
    };

    render() {
        let {addButton, show, search, tags, back} = this.state;

        const showOptions = [{value: '1', label: 'All'}, {value: '2', label: "Yura"}];

        return (
            <header className={styles.header}>
                {(back) &&
                <Link to={"/login"}>
                    <div className={styles.back}>
                        <span> </span>
                        <span> </span>
                    </div>
                </Link>
                }
                <h3 className={styles.tiitle}>{this.state.page__name}</h3>
                <div className={styles.filters_wrap}>
                    {(show) &&
                    <div className={styles.filters}>
                        <span>Show</span>
                        <Dropdown
                            list={showOptions}
                            onSelectSort={this.onSelectSort}/>
                    </div>
                    }
                    {(tags) &&
                    <div className={styles.filters}>
                        <span>Tags</span>
                        <Dropdown
                            list={showOptions}
                            onSelectSort={this.onSelectSort}/>
                    </div>
                    }
                </div>
                {(search) &&
                <React.Fragment>
                    <Search handlerSearch={this.handlerSearch}/>
                </React.Fragment>
                }
                {(addButton) &&
                <Link
                    className={styles.new}
                    to={addButton.link}
                    onClick={this.props.handelerSubmit && this.props.handelerSubmit}
                >
                    {addButton.text}
                </Link>}
            </header>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.user
    }
}

export default connect(mapStateToProps)(Header);


