import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Home from './components/Home/home'
import Layout from "./hoc/layout";
import Logout from "./components/Admin/logout";
import Login from "./containers/Admin/login";
import Forgot_Pwd from "./containers/Admin/forgot_pwd";
import Auth from './hoc/auth'
import Updates from "./components/Updates/Updates";
import UpdatesRecord from "./components/Updates/Updates record/updates_record";
import addRecord from "./components/Updates/createRecord";
import Team from "./components/Team/team";
import MyAccount from "./components/My account/my_account";
import ChangeMail from "./components/My account/changeMailPassord";
import ProfileUpdates from "./components/My account/profileUpdates";


// import Nav from "./hoc/Sidenav/sidenav";
// import Office from "./components/Office/office";
// import Resources from "./components/Resources/resources";
// import Chat from "./components/Chat/chat";



const Routes = () => {

    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Auth(Home, true)}    />
                <Route path="/updates" exact component={Auth(Updates, true)}/>
                <Route path="/updates/:id" exact component={Auth(UpdatesRecord, null)}/>
                <Route path="/update/create" exact component={Auth(addRecord, true)}/>
                <Route path="/team" exact component={Auth(Team, true)}/>
                <Route path="/my_account" exact component={Auth(MyAccount, true)}/>
                <Route path="/my_account/change_mail" exact component={Auth(ChangeMail, null)}/>
                <Route path="/my_account/updates" exact component={Auth(ProfileUpdates, null)}/>
                <Route path="/my_account/logout" exact component={Auth(Logout, true)}/>
                <Route path="/login" exact component={Auth(Login, false)}/>
                <Route path="/forgot_pwd" exact component={Auth(Forgot_Pwd, false)}/>
                <Route from='*' to='/'  component={Auth(Home, true)}   />
            </Switch>
        </Layout>
    );
};

export default Routes;
