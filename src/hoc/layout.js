import React from 'react';
import Nav from "./Sidenav/sidenav";
import {Col, Row} from "../components/WidgetsUI/container";

const Layout = (props) => {
    // let handleLogin = (loginHandler=false) => {
    //     console.log(loginHandler)
    //     return loginHandler
    // };

    return (
        <Row padding={false}>
            <Col mobile={"2"} padding={false} >
                <Nav />
            </Col>

            <Col mobile={"10"} padding={false}>
                {props.children}
            </Col>
        </Row>


    );
};

export default Layout;
