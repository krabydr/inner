import React, {Component} from 'react';
import SideNavItems from "./sidenav_items";
import {connect} from "react-redux";
import sidebar from '../../scss/components/_sidebar.scss';


class Nav extends Component {


    render() {

        return (
            <div className={`${sidebar["sidebar"]}`}>
                <header>
                    {/*<Link to="/" className="logo">*/}
                    Inner Circle
                    {/*</Link>*/}
                </header>
                <SideNavItems itemStyle={sidebar.navItem}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.user
    }
}

export default connect(mapStateToProps)(Nav);
