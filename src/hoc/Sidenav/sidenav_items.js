import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {navItems,active} from '../../scss/components/_sidebar.scss';
const SideNavItems = (props) => {
    /* Docs
     Items: object with all routs
        type: Class for css
        text: Link texts
        link: Route link
        restricted: Not logged user can see this item
        accesses: Are this block have been pay

     */
    const items = [
        {
            type: navItems,
            text: 'Dashboard',
            link: '/',
            restricted: false,
            accesses: true
        },
        {
            type: navItems,
            text: 'Updates',
            link: '/updates',
            restricted: false,
            accesses: true,
        },
        {
            type: navItems,
            text: 'Team',
            link: '/team',
            restricted: false,
            accesses: true
        },
        {
            type: navItems,
            text: 'Agenda',
            link: '/login',
            restricted: false,
        },
        {
            type: navItems,
            text: 'Office',
            link: '/office',
            restricted: false
        },
        {
            type: navItems,
            text: 'Resources',
            link: '/resources',
            restricted: false
        },
        {
            type: navItems,
            text: 'Chat',
            link: '/chat',

            restricted: false
        },
        {
            type: navItems,
            text: 'My account',
            link: '/my_account',
            restricted: false,
            accesses: true
        },
    ];


    const element = (item, i , url) => (
        <div key={i} className={`${item.type} ${item.link === url ? active : null}` }>
            <Link to={item.link}>
                {item.text}
            </Link>
        </div>
    );
    const showItems = () => {
        const url = window.location.pathname;
        // const {pathname} = this.props.location;
         return props.user.login ?
            items.map((item, i) => {
                if (props.user.login.isAuth && item.accesses) {
                    return element(item, i, url);
                } else {
                    return item.restricted ? element(item, i , url) : null;
                }
                // return element(item, i)
            })
            : null
    };

    return (
        <React.Fragment>
            {showItems()}
        </React.Fragment>
    );
};

function mapStateToProps(state) {
    return {
        user: state.user,

    }
}

export default connect(mapStateToProps)(SideNavItems);



