import React, {Component} from 'react';
import {auth} from '../actions';
import {connect} from 'react-redux';

import loader from '../scss/components/_loader.scss';

export default function (ComposedClass, reload) {
    class AuthenticationCheck extends Component {
        state = {
            loading: true
        };

        componentWillMount() {
            this.props.dispatch(auth());
        }

        componentWillReceiveProps(nextProps) {

                this.setState({
                    loading: false
                });


            if (!nextProps.user.login.isAuth) {
                if (reload) {
                    this.props.history.push('/login');
                }
            } else {
                if (reload === false) {
                    this.props.history.push('/');
                }
            }

        }

        render() {
            if (this.state.loading) {
                return (
                    <div className={loader.loader}>
                        <div className={loader.loader_wrap}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="960" height="80" fill="DodgerBlue"
                                 viewBox="0 0 120 10">
                                <path d="M0,5 C20,-10 40,20 60,5 v5 H0"/>
                                <path transform="translate(60)" d="M0,5 C20,-10 40,20 60,5 v5 H0"/>
                            </svg>
                        </div>
                    </div>
                );
            }
            return (
                <ComposedClass {...this.props} user={this.props.user}/>
            )
        }
    }

    function mapStateToProps(state) {

        return {
            user: state.user
        }
    }

    return connect(mapStateToProps)(AuthenticationCheck)
}